package com.example.linkedliabackend.student.skills;

import com.example.linkedliabackend.student.StudentEntity;
import com.example.linkedliabackend.student.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
@AllArgsConstructor
public class StudentSkillService {
    @Autowired
    StudentSkillRepository studentSkillRepository;
    @Autowired
    StudentRepository studentRepository;

    @Transactional
    public Optional<StudentSkillEntity> addSkillToStudent(String username, CreateStudentSkill createSkill) {
        StudentEntity studentEntity = studentRepository.findByUsername(username).orElse(null);
        StudentSkillEntity skillEntity = new StudentSkillEntity(
                UUID.randomUUID().toString(),
                createSkill.getSkill(),
                createSkill.getLevel(),
                studentEntity);

        return Optional.of(studentSkillRepository.save(skillEntity));
    }

    public Optional<StudentSkillEntity> deleteSkillFromStudent(String username, String skillId) {
        StudentEntity student = studentRepository.findByUsername(username).orElse(null);
        StudentSkillEntity skill = studentSkillRepository.findById(skillId).orElse(null);

        if(student.getStudentSkillEntities().contains(skill)){
            skill.setStudentEntity(null);
            studentSkillRepository.delete(skill);
            return Optional.of(skill);
        }

        return Optional.empty();
    }

    public Optional<StudentSkillEntity> updateStudentSkill(String id, String skillId, CreateStudentSkill updateSkill) {
        StudentEntity student = studentRepository.findById(id).orElse(null);
        StudentSkillEntity skill = studentSkillRepository.findById(skillId).orElse(null);
        if(student.getId().equals(skill.getStudentEntity().getId())){
            skill.setSkill(updateSkill.getSkill());
            skill.setLevel(updateSkill.getLevel());
            return Optional.of(studentSkillRepository.save(skill));
        }

        return Optional.empty();
    }

    public List<StudentSkillEntity> getAllSkills(String username) {
        StudentEntity studentEntity = studentRepository.findByUsername(username).orElse(null);
        if (studentEntity != null) {
            return studentEntity.getStudentSkillEntities();
        }
        return null;
    }
}
