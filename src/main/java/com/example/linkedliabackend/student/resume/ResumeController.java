package com.example.linkedliabackend.student.resume;
import com.example.linkedliabackend.student.CommonDTO;
import com.example.linkedliabackend.student.StudentDTO;
import com.example.linkedliabackend.student.StudentEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
@RestController
@RequestMapping("/students")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ResumeController {
    ResumeService resumeService;
    CommonDTO commonDTO = new CommonDTO();

    public ResumeController(ResumeService resumeService) {
        this.resumeService = resumeService;
    }
    @PostMapping(value = "/{id}/addresume",consumes = {MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE})
    public StudentDTO addProfilePic(@PathVariable("id") String id, @RequestPart("file") MultipartFile multipartFile) throws IOException {
        StudentEntity studentEntity = resumeService.addResume(id,multipartFile);
        return commonDTO.toStudentDTO(studentEntity);
    }
}

