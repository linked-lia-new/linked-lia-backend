package com.example.linkedliabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinkedliabackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(LinkedliabackendApplication.class, args);
    }

}
