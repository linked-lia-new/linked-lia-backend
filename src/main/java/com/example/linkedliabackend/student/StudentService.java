package com.example.linkedliabackend.student;


import com.example.linkedliabackend.KeycloakConfig;
import com.example.linkedliabackend.jwt.KeyCloakToken;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
@AllArgsConstructor
@NoArgsConstructor
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    KeyCloakToken keyCloakToken;

    KeycloakConfig keycloakConfig;

    String token;

    public StudentService(KeycloakConfig keycloakConfig) {
        this.keycloakConfig = keycloakConfig;
    }

    public StudentService(KeyCloakToken keyCloakToken) {
        this.keyCloakToken = keyCloakToken;
    }

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    private static CredentialRepresentation createPasswordCredentials(String password) {
        CredentialRepresentation passwordCredentials = new CredentialRepresentation();
        passwordCredentials.setTemporary(false);
        passwordCredentials.setType(CredentialRepresentation.PASSWORD);
        passwordCredentials.setValue(password);
        return passwordCredentials;
    }

    public UserRepresentation addUser(CreateStudent createStudent) {
        UsersResource usersResource = KeycloakConfig.getInstance().realm(KeycloakConfig.realm).users();
        CredentialRepresentation credentialRepresentation = createPasswordCredentials(createStudent.getPassword());

        UserRepresentation kcUser = new UserRepresentation();
        kcUser.setUsername(createStudent.getUsername());

        kcUser.setCredentials(Collections.singletonList(credentialRepresentation));
        kcUser.setFirstName(createStudent.getFirstName());
        kcUser.setLastName(createStudent.getLastName());
        kcUser.setEmail(createStudent.getEmail());
        kcUser.setEnabled(true);
        kcUser.setEmailVerified(false);

        Response response = usersResource.create(kcUser);

        createNewStudent(createStudent);

        System.out.println("response = " + response.getEntity());
        return kcUser;
    }

    public Optional<StudentEntity> createNewStudent(CreateStudent createStudent) {
        StudentEntity studentEntity = new StudentEntity(
                UUID.randomUUID().toString(),
                createStudent.getUsername(),
                createStudent.getFirstName(),
                createStudent.getLastName(),
                createStudent.getPassword(),
                createStudent.getEmail()
        );
        studentRepository.save(studentEntity);
        return Optional.of(studentEntity);
    }

    public List<StudentEntity> getAllStudents() {
        return (List<StudentEntity>) studentRepository.findAll();
    }

    public Optional<StudentEntity> deleteStudent(String id) {
        StudentEntity studentEntity = studentRepository.findById(id).orElse(null);
        studentRepository.delete(studentEntity);
        return Optional.of(studentEntity);
    }

    public Optional<StudentEntity> findStudent(String id) {
        StudentEntity studentEntity = studentRepository.findById(id).orElse(null);
        assert studentEntity != null;
        return Optional.of(studentEntity);
    }

    public Optional<StudentEntity> updateStudent(String username, UpdateStudent updateStudent) {
        StudentEntity studentEntity = studentRepository.findByUsername(username).orElse(createUserFromKeycloak(username));
        studentEntity.setFirstName(updateStudent.getFirstName());
        studentEntity.setLastName(updateStudent.getLastName());
        studentEntity.setPassword(updateStudent.getPassword());
        studentEntity.setEmail(updateStudent.getEmail());
        return Optional.of(studentRepository.save(studentEntity));
    }

    private StudentEntity createUserFromKeycloak(String username) {
        //get user from keycloak
        //Store user in database
        return null;

    }

    public Mono<KeyCloakToken> acquire(String keyCloakBaseUrl, String realm, String clientId, String username, String password) {
        WebClient webClient = WebClient.builder()
                .baseUrl(keyCloakBaseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();

        Mono<KeyCloakToken> keyCloakToken = webClient.post()
                .uri("auth/realms/" + realm + "/protocol/openid-connect/token")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData("grant_type", "password")
                        .with("client_id", clientId)
                        .with("username", username)
                        .with("password", password)
                        .with("access_token", ""))
                .retrieve()
                .bodyToFlux(KeyCloakToken.class)
                .onErrorMap(e -> new Exception("Failed to aquire token", e))
                .last();
        token = keyCloakToken.map(KeyCloakToken::getAccessToken).block();
        return keyCloakToken;
    }

    public Mono<String> createNewUser(String keyCloakBaseUrl, String realm, KeycloakUserDTO keycloakUserDTO) {
        WebClient webClient = WebClient.builder()
                .baseUrl(keyCloakBaseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .build();

        Mono<String> keycloakUser = webClient.post()
                .uri("auth/admin/realms/" + realm + "/users")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(keycloakUserDTO)
                .retrieve()
                .bodyToMono(String.class)
                .onErrorMap(e -> new Exception("Failed to create user", e));

        System.out.println("StudentService.createNewUser USER = " + keycloakUser);
        return keycloakUser;
    }

    public static void main(String[] args) {

        StudentService studentService = new StudentService();

        UserRepresentation userRepresentation = studentService.addUser(new CreateStudent("Olli5",
                "Olle", "jhj", "password", "olle5@gamil.com"));

        System.out.println("userRepresentation = " + userRepresentation);

//        studentService.acquire("http://localhost:8000/",
//                "linkedlia",
//                "student-client",
//                "student1",
//                "password").block();
//
//        studentService.createNewUser("http://localhost:8000/",
//                "linkedlia", new KeycloakUserDTO("hhhj@gmail", true, "MAJS", "Togge", "Majs")).block();
////Email måste vara unik.
    }

}
