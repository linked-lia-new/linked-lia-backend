package com.example.linkedliabackend.student.education;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CreateStudentEducation {
    String school;
    String field;
    String degree;
    String city;
    String startDate;
    String endDate;
}
