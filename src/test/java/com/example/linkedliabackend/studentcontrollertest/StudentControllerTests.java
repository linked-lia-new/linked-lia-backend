package com.example.linkedliabackend.studentcontrollertest;

import com.example.linkedliabackend.student.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.security.Principal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
@ActiveProfiles("integrationtest")
public class StudentControllerTests {
    @Autowired
    StudentController studentController;

    @Autowired
    StudentRepository studentRepository;

    @AfterEach
    void tearDown() {
        studentRepository.deleteAll();
        //Rensa testernas users från keycloak
    }

    @Test
    void test_addStudent_success() {
        //Given
        studentRepository.save(new StudentEntity("1", "kcId", "Shakir", "Zahedi",
                "12345", "ami@gmai.com"));

        // When
        List<StudentDTO> allStudents = studentController.getAllStudents();

        //Then
        assertEquals(1, allStudents.size());
        assertEquals("Shakir", allStudents.get(0).getFirstName());
    }

    @Test
    void test_update_student() {
        //Given
        StudentDTO student = studentController.createNewStudent(new CreateStudent("Majs", "kcId", "Kolv",
                "12345", "ami@gmai.com"));
        Principal principal = () -> "14ed57e9-fbab-4bc8-9ffa-1d1dcd0f89e3";

        //When
        studentController.updateStudent(principal, new UpdateStudent("hej", "då", "12334", "jagödordögh"));
        StudentDTO updatedStudent = studentController.findStudent(student.getId());

        //Then
        assertEquals("hej", updatedStudent.getFirstName());
    }
    @Test
    void test_add_student() {
        //Given
        UserRepresentation student = studentController.createUser(new CreateStudent("Majs2", "kcId", "Kolv",
                "12345", "ami@gmai.com"));

        //When

        //Then
        //Kontrollera att usern sparas i databasen!!
    }



}
