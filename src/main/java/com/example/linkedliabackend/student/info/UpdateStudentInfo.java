package com.example.linkedliabackend.student.info;

import lombok.*;


@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateStudentInfo {
    String phone;
    String address;
    String postcode;
    String city;
    String linkedIn;
    String gitLab;
    String bio;
}
