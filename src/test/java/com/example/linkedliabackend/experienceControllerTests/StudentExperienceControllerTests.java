package com.example.linkedliabackend.experienceControllerTests;

import com.example.linkedliabackend.student.StudentEntity;
import com.example.linkedliabackend.student.StudentRepository;
import com.example.linkedliabackend.student.experience.StudentExperienceController;
import com.example.linkedliabackend.student.experience.StudentExperienceDTO;
import com.example.linkedliabackend.student.experience.StudentExperienceEntity;
import com.example.linkedliabackend.student.experience.StudentExperienceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.security.Principal;
import java.util.List;


@SpringBootTest
@ActiveProfiles("integrationtest")
public class StudentExperienceControllerTests {
    @Autowired
    StudentExperienceController studentExperienceController;
    @Autowired
    StudentExperienceRepository studentExperienceRepository;
    @Autowired
    StudentRepository studentRepository;

    Principal principal = () -> "14ed57e9-fbab-4bc8-9ffa-1d1dcd0f89e3";

    @BeforeEach
    void setUp() {
        studentExperienceRepository.deleteAll();
    }

    @Test
    void test_addExp_success() {
        //Given
        StudentEntity student = studentRepository.findByUsername("").orElse(null);
        studentExperienceRepository.save(new StudentExperienceEntity("1", "SEB", "CEO", "BOSS",
                "Stockholm", "2020", "2021", student));


        // When
        List<StudentExperienceDTO> allExperiences = studentExperienceController.getAllExperience(principal);
        System.out.println("EXPERIENCE " + allExperiences);

        // FORTSÄTT HÄR... OM DU VILL
    }
}
