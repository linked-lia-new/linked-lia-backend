package com.example.linkedliabackend.student;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface StudentRepository extends CrudRepository<StudentEntity, String> {

    Optional<StudentEntity> findByUsername(String username);

}
