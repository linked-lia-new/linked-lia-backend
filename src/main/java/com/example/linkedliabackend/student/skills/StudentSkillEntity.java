package com.example.linkedliabackend.student.skills;

import com.example.linkedliabackend.student.StudentEntity;
import lombok.*;

import javax.persistence.*;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="student_skill")
public class StudentSkillEntity {
    @Id
    String id;
    @Column(name = "skill")
    String skill;
    @Enumerated(EnumType.ORDINAL)
    SkillLevel level;

    @ManyToOne
    @JoinColumn(name = "studentEntity_id")
    StudentEntity studentEntity;
}
