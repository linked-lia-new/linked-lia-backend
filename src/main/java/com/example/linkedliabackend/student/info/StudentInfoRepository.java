package com.example.linkedliabackend.student.info;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentInfoRepository extends CrudRepository<StudentInfoEntity, String> {
}
