package com.example.linkedliabackend.student.experience;


import com.example.linkedliabackend.student.CommonDTO;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/experiences")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentExperienceController {

    StudentExperienceService studentExperienceService;
    CommonDTO commonDTO = new CommonDTO();

    public StudentExperienceController(StudentExperienceService studentExperienceService) {
        this.studentExperienceService = studentExperienceService;
    }
/*    public StudentExperienceController(StudentService studentService, StudentExperienceService studentExperienceService) {
        this.studentService = studentService;
        this.studentExperienceService = studentExperienceService;
    }*/

    @GetMapping("/allexperience")
    public List<StudentExperienceDTO> getAllExperience(Principal principal) {
        return studentExperienceService.getAllExperience(principal.getName())
                .stream()
                .map(commonDTO::toStudentExperienceDTO)
                .collect(Collectors.toList());
    }

    @PostMapping("/addExp")
    public StudentExperienceDTO addExperience(Principal principal, @RequestBody CreateStudentExperience createStudentExperience) {
        return studentExperienceService.addExperience(principal.getName(), createStudentExperience)
                .map(commonDTO::toStudentExperienceDTO)
                .orElse(null);
    }

    @DeleteMapping("/{exp_id}/delete")
    public StudentExperienceDTO deleteExperienceFromStudent(Principal principal, @PathVariable(value = "exp_id") String expId) {
        return studentExperienceService.deleteExperience(principal.getName(), expId)
                .map(commonDTO::toStudentExperienceDTO)
                .orElse(null);
    }

/*    public static void main(String[] args) {

        StudentExperienceController studentExperienceController = new StudentExperienceController();
        Principal principal = () -> "14ed57e9-fbab-4bc8-9ffa-1d1dcd0f89e3";

      List<StudentExperienceDTO> allExp = studentExperienceController.getAllExperience(principal);

        System.out.println("ALL ** " + allExp);
    }*/

}
