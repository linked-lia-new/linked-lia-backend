package com.example.linkedliabackend.student.experience;


import com.example.linkedliabackend.student.StudentEntity;
import com.example.linkedliabackend.student.StudentRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
//@NoArgsConstructor
public class StudentExperienceService {

    StudentRepository studentRepository;
//    @Autowired
    StudentExperienceRepository studentExperienceRepository;

    public StudentExperienceService(StudentRepository studentRepository, StudentExperienceRepository studentExperienceRepository) {
        this.studentRepository = studentRepository;
        this.studentExperienceRepository = studentExperienceRepository;
    }

    public StudentExperienceEntity updateExperience(String id, UpdateStudentExperience updateStudentExperience) {
        return null;
    }

    @Transactional
    public Optional<StudentExperienceEntity> addExperience(String username, CreateStudentExperience createStudentExperience) {
        StudentEntity studentEntity = studentRepository.findByUsername(username).orElse(null);
        StudentExperienceEntity studentExperienceEntity = new StudentExperienceEntity(
                UUID.randomUUID().toString(),
                createStudentExperience.getCompany(),
                createStudentExperience.getPosition(),
                createStudentExperience.getDescription(),
                createStudentExperience.getCity(),
                createStudentExperience.getStartDate(),
                createStudentExperience.getEndDate(),
                studentEntity);

        return Optional.of(studentExperienceRepository.save(studentExperienceEntity));
    }
    @Transactional
    public Optional<StudentExperienceEntity> deleteExperience(String username, String expId) {
        StudentEntity student = studentRepository.findByUsername(username).orElse(null);
        StudentExperienceEntity studentExperience = studentExperienceRepository.findById(expId).orElse(null);
  /*      assert student != null;
        assert studentExperience != null;*/
        if (student.getStudentExperienceEntity().contains(studentExperience)) {
            studentExperience.setStudentEntity(null);
            studentExperienceRepository.delete(studentExperience);
            return Optional.of(studentExperience);
        }
        return Optional.empty();
    }

    public List<StudentExperienceEntity> getAllExperience(String username) {
        StudentEntity studentEntity = studentRepository.findByUsername(username).orElse(null);
        assert studentEntity != null;
        return studentEntity.getStudentExperienceEntity();
    }

/*    public static void main(String[] args) {

        StudentExperienceService studentExperienceService = new StudentExperienceService();

        Principal principal = () -> "14ed57e9-fbab-4bc8-9ffa-1d1dcd0f89e3";

        List<StudentExperienceEntity> allExp = studentExperienceService.getAllExperience("14ed57e9-fbab-4bc8-9ffa-1d1dcd0f89e3");

        System.out.println("ALL ** " + allExp.toString());
    }*/

}
