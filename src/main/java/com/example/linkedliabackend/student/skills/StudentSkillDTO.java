package com.example.linkedliabackend.student.skills;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder(toBuilder = true)
public class StudentSkillDTO {
    String id;
    String skill;
    SkillLevel level;

    public StudentSkillDTO(String id, String skill) {
        this.id = id;
        this.skill = skill;
    }
}

