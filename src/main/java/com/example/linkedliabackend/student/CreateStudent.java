package com.example.linkedliabackend.student;


import lombok.AllArgsConstructor;
import lombok.Value;


@AllArgsConstructor
@Value
public class CreateStudent {
    String username;
    String firstName;
    String lastName;
    String password;
    String email;
}

