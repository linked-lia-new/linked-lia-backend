package com.example.linkedliabackend.student.education;

import com.example.linkedliabackend.student.CommonDTO;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/educations")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentEducationController {

    StudentEducationService studentEducationService;
    CommonDTO commonDTO = new CommonDTO();

    public StudentEducationController(StudentEducationService studentEducationService) {
        this.studentEducationService = studentEducationService;
    }
    @PostMapping("/addeducation")
   // public StudentEducationDTO addEducation(@PathVariable("id") String id, @RequestBody CreateStudentEducation createStudentEducation){
    public StudentEducationDTO addEducation(@RequestBody CreateStudentEducation createStudentEducation, Principal principal){

        System.out.println("ID? : " + principal.getName() );
        return  studentEducationService.addEducation(principal.getName(), createStudentEducation)
                .map(commonDTO::toStudentEducationDTO)
                .orElse(null);
    }

    @GetMapping("/alleducations")
    public List<StudentEducationDTO> getAllEducations(Principal principal) {
        return studentEducationService.getAllEducations(principal.getName())
                .stream()
                .map(commonDTO::toStudentEducationDTO)
                .collect(Collectors.toList());
    }

    @DeleteMapping("/{education_id}/delete")
    public StudentEducationDTO deleteEducation(Principal principal, @PathVariable(value= "education_id") String edId){
        return  studentEducationService.deleteEducation(principal.getName(), edId)
                .map(commonDTO::toStudentEducationDTO)
                .orElse(null);
    }
    @PutMapping("/{id}/educations/{eid}/")
    public StudentEducationDTO updateEducation(@PathVariable("id") String id, @PathVariable("eid") String eid,
                                               @RequestBody UpdateStudentEducation updateStudentEducation){
        return  studentEducationService.updateEducation(id,eid,updateStudentEducation)
                .map(commonDTO::toStudentEducationDTO)
                .orElse(null);
    }
}

