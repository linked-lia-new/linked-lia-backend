package com.example.linkedliabackend.student;

import lombok.Value;


@Value
public class KeycloakUserDTO {
    String email;
    boolean enabled;
    String firstName;
    String lastName;
    String username;
}

// "firstName":"Sergey","lastName":"Kargopolov", "email":"test@test.com", "enabled":"true", "username":"app-user"