package com.example.linkedliabackend.student.info;

import com.example.linkedliabackend.student.StudentEntity;
import com.example.linkedliabackend.student.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;


@Service
@AllArgsConstructor
public class StudentInfoService {
    @Autowired
    StudentInfoRepository studentInfoRepository;
    @Autowired
    StudentRepository studentRepository;

    @Transactional
    public Optional<StudentInfoEntity> updateInfo(String id, UpdateStudentInfo updateInfo) {
        StudentEntity studentEntity = studentRepository.findById(id).orElse(null);
        StudentInfoEntity studentInfoEntity = new StudentInfoEntity(UUID.randomUUID().toString(),
                updateInfo.getPhone(),
                updateInfo.getAddress(),
                updateInfo.getPostcode(),
                updateInfo.getCity(),
                updateInfo.getLinkedIn(),
                updateInfo.getGitLab(),
                updateInfo.getBio(),
                studentEntity);
        studentEntity.setStudentInfoEntity(studentInfoEntity);
        studentRepository.save(studentEntity);
        return Optional.of(studentInfoEntity);
    }
}
