package com.example.linkedliabackend.student.info;

import com.example.linkedliabackend.student.CommonDTO;
import com.example.linkedliabackend.student.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentInfoController {
    @Autowired
    StudentInfoService studentInfoService;
    @Autowired
    StudentService studentService;
    CommonDTO commonDTO = new CommonDTO();


    public StudentInfoController(StudentInfoService studentInfoService, StudentService studentService) {
        this.studentInfoService = studentInfoService;
        this.studentService = studentService;
    }

    @PostMapping("/{id}/addInfo")
    public StudentInfoDTO updateInfo(@PathVariable ("id") String id, @RequestBody UpdateStudentInfo updateInfo){
        return studentInfoService.updateInfo(id, updateInfo)
                .map(commonDTO::toInfoDTO)
                .orElse(null);
    }

/*    private StudentInfoDTO toInfoDTO(StudentInfoEntity studentInfoEntity){
        return new StudentInfoDTO(studentInfoEntity.getId(),
                studentInfoEntity.getPhone(),
                studentInfoEntity.getAddress(),
                studentInfoEntity.getPostcode(),
                studentInfoEntity.getCity(),
                studentInfoEntity.getLinkedIn(),
                studentInfoEntity.getGitLab(),
                studentInfoEntity.getBio());
    }*/
}
