package com.example.linkedliabackend.student.skills;


import com.example.linkedliabackend.student.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;


@AllArgsConstructor
@RestController
@RequestMapping("skills")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentSkillController {
    @Autowired
    StudentService studentService;
    @Autowired
    StudentSkillService studentSkillService;

    @GetMapping("get_all_skills")
    public List<StudentSkillDTO> getSkills(Principal principal) {
        return studentSkillService.getAllSkills(principal.getName())
                .stream()
                .map(this::skillToDTO)
                .collect(Collectors.toList());
    }

    @PostMapping("/add_skill")
    public StudentSkillDTO addSkill(Principal principal, @RequestBody CreateStudentSkill createStudentSkill) {
        return studentSkillService.addSkillToStudent(principal.getName(), createStudentSkill)
                .map(this::skillToDTO)
                .orElse(null);
    }

    @PostMapping("/{id}/{skill_id}/update_skill")
    public StudentSkillDTO updateSkill(@PathVariable("id") String id,
                                       @PathVariable("skill_id") String skillId,
                                       @RequestBody CreateStudentSkill updateSkill){
        return studentSkillService.updateStudentSkill(id, skillId, updateSkill)
                .map(this::skillToDTO)
                .orElse(null);
    }

    @DeleteMapping("/{skill_id}/delete_skill")
    public StudentSkillDTO deleteSkill(Principal principal, @PathVariable("skill_id") String skillId) {
        return studentSkillService.deleteSkillFromStudent(principal.getName(), skillId)
                .map(this::skillToDTO)
                .orElse(null);
    }

    private StudentSkillDTO skillToDTO(StudentSkillEntity skillEntity){
        return new StudentSkillDTO(
                skillEntity.getId(),
                skillEntity.getSkill(),
                skillEntity.getLevel());
    }
}
