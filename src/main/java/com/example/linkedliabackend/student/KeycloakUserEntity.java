package com.example.linkedliabackend.student;

import lombok.Setter;
import lombok.Value;


@Value
@Setter
public class KeycloakUserEntity {
    String keycloakId;
    String firstName;
    String lastName;
    String email;
    String enabled;
    String username;
}
